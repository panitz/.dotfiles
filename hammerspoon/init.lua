-- https://github.com/mattorb/dotfiles

local hyper = { "cmd", "alt", "ctrl", "shift" }
local modKeys = { "cmd", "alt", "ctrl" }

hs.hotkey.bind(modKeys, "W", function()
    hs.alert.show(
       "Hello World!",
       {
          textFont= "Comic Sans MS",
          textSize=72,
          fadeOutDuration=1
       }
    )
end)

local applicationHotkeys = {
    b = 'Google Chrome',
    t = 'iTerm',
    c = 'Visual Studio Code',
    g = "GoLand"
}

for key, app in pairs(applicationHotkeys) do
    hs.hotkey.bind(modKeys, key, function()
      hs.application.launchOrFocus(app)
    end)
end

-- Window layout bindings

-- +-----------------+
-- |  HERE  |        |
-- +--------+        |
-- |                 |
-- +-----------------+
hs.hotkey.bind(hyper, "Q", function() 
    local win = hs.window.focusedWindow()
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()

    f.x = max.x
    f.y = max.y
    f.w = max.w/2
    f.h = max.h/2
    win:setFrame(f)
end)

-- +-----------------+
-- |                 |
-- +--------+        |
-- |  HERE  |        |
-- +-----------------+
hs.hotkey.bind(hyper, "Y", function() 
    local win = hs.window.focusedWindow()
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()

    f.x = max.x
    f.y = max.y + (max.h / 2)
    f.w = max.w/2
    f.h = max.h/2
    win:setFrame(f)
end)

-- +-----------------+
-- |                 |
-- |        +--------|
-- |        |  HERE  |
-- +-----------------+
hs.hotkey.bind(hyper, "X", function() 
    local win = hs.window.focusedWindow()
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()

    f.x = max.x + (max.w / 2)
    f.y = max.y + (max.h / 2)
    f.w = max.w/2
    f.h = max.h/2
    win:setFrame(f)
end)

-- +-----------------+
-- |        |  HERE  |
-- |        +--------|
-- |                 |
-- +-----------------+
hs.hotkey.bind(hyper, "E", function() 
    local win = hs.window.focusedWindow()
    local f = win:frame()
    local screen = win:screen()
    local max = screen:frame()

    f.x = max.x + (max.w / 2)
    f.y = max.y
    f.w = max.w/2
    f.h = max.h/2
    win:setFrame(f)
end)

hs.hotkey.bind(hyper, "left", function()
-- size focused window to left half of display
local win = hs.window.focusedWindow()
local f = win:frame()
local screen = win:screen()
local max = screen:frame()

f.x = max.x
f.y = max.y
f.w = max.w / 2
f.h = max.h
win:setFrame(f)
end)

-- +-----------------+
-- |      |          |
-- | HERE |          |
-- |      |          |
-- +-----------------+
hs.hotkey.bind(hyper, "a", function()
-- size focused window to left third of display
local win = hs.window.focusedWindow()
local f = win:frame()
local screen = win:screen()
local max = screen:frame()

f.x = max.x
f.y = max.y
f.w = max.w * 0.4
f.h = max.h
win:setFrame(f)
end)

-- +--------------+
-- |  |        |  |
-- |  |  HERE  |  |
-- |  |        |  |
-- +---------------+
hs.hotkey.bind(hyper, "S", function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    local screen = win:screen()
    local max = screen:fullFrame()
  
    f.x = max.x + (max.w / 5)
    f.w = max.w * 3/5
    f.y = max.y
    f.h = max.h
    win:setFrame(f)
end)

-- +-----------------+
-- |      |          |
-- |      |   HERE   |
-- |      |          |
-- +-----------------+
hs.hotkey.bind(hyper, "d", function()
-- size focused window to right third of display
local win = hs.window.focusedWindow()
local f = win:frame()
local screen = win:screen()
local max = screen:frame()

f.x = max.x + (max.w * 0.4)
f.y = max.y
f.w = max.w * 0.6
f.h = max.h
win:setFrame(f)
end)

hs.hotkey.bind(hyper, "right", function()
-- size focused window to right half of display
local win = hs.window.focusedWindow()
local f = win:frame()
local screen = win:screen()
local max = screen:frame()

f.x = max.x + (max.w / 2)
f.y = max.y
f.w = max.w / 2
f.h = max.h
win:setFrame(f)
end)

hs.hotkey.bind(hyper, "return", function()
-- size focused window to size of desktop
local win = hs.window.focusedWindow()
local f = win:frame()
local screen = win:screen()
local max = screen:frame()

f.x = max.x
f.y = max.y
f.w = max.w
f.h = max.h
win:setFrame(f)
end)

hs.hotkey.bind(hyper, "up", function()
-- size focused window to top half of display
local win = hs.window.focusedWindow()
local f = win:frame()
local screen = win:screen()
local max = screen:frame()

f.x = max.x
f.y = max.y
f.w = max.w
f.h = max.h / 2
win:setFrame(f)
end)

hs.hotkey.bind(hyper, "down", function()
-- size focused window to bottom half of display
local win = hs.window.focusedWindow()
local f = win:frame()
local screen = win:screen()
local max = screen:frame()

f.x = max.x
f.y = max.y + (max.h / 2)
f.w = max.w
f.h = max.h / 2
win:setFrame(f)
end)



hs.hotkey.bind(hyper, "F", function()
-- toggle the focused window to full screen (workspace)
local win = hs.window.focusedWindow()
win:setFullScreen(not win:isFullScreen())
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "right", function()
-- move the focused window one display to the right
local win = hs.window.focusedWindow()
win:moveOneScreenEast()
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "left", function()
-- move the focused window one display to the left
local win = hs.window.focusedWindow()
win:moveOneScreenWest()
end)